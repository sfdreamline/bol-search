global.__basePath       = process.cwd() + '/';
const constant   = require(__basePath +'/constant_test');
var querystring         = require('querystring');
const exec = require('await-exec');
const fs                = require('fs');
const async             = require('async');
const moment            = require('moment');
const csvParser         = require('csv-parser');  
const createCsvWriter   = require('csv-writer').createObjectCsvWriter; 
var todayDate           = moment().subtract(1, "days").format('YYYY_MM_DD');
const nodeEnv           = process.env.NODE_ENV || 'development';
console.log("current env is "+nodeEnv);
const loggerObj         = require(__basePath + 'logger_test');
var filePath            = loggerObj.csvFilePath;
const publicfolder="/home/ubuntu/SearchPdf/public/"
//var  MongoDB   	= require('database.js');
//let mongoose = require('mongoose');
const storedFileData = [];
var nodemailer = require('nodemailer');

class ReportGenerator {
    /**
    * Create a ReportGenerator Object.
    * @function constructor
    * @param {object} instance - The object instance.
    */
    constructor(){
		//console.log("current env is "+nodeEnv);
    }

   
    accessDate(path){
        const options = {
            url: constant.chatTranscriptDetails.url+"?ChatId="+workId,
            method: 'GET',
            headers: {
                'Authorization': 'Bearer '+accessToken
            }
        };
        var chatTranscriptDetails = {};
        return new Promise((resolve, reject) => {
            request(options, function (error, response){
                if(error){
                    console.log("error is:"+error);
                    reject(error);
                } else {
                    var parsedBody = JSON.parse(response.body);
                    console.log("body is:"+response.body);
                    
                    if(parsedBody.strResponseData != undefined){
                        chatTranscriptDetails['Work Id'] = workId;
                        chatTranscriptDetails['transcript'] = parsedBody.strResponseData;
                    }
                    resolve(chatTranscriptDetails);
                }
            });
        });
    }



 
    async generateChatTranscriptReport (accessToken, workIdObj){
        const classObj = this;
        var chatTranscriptDetailsJson = [];
        for (let index = 0; index < workIdObj.length; index++){
            chatTranscriptDetailsJson.push(await classObj.chatTranscript(accessToken, workIdObj[index]));
        }  
        return await Promise.all(chatTranscriptDetailsJson); // resolving all promises
    }


    /**
    * Read CSV File Data
    * @function readCSVFile
    * @return WorkId Object.
    */
    readCSVFile(){
        var csvFilePath = new Array();
        var i = -1;
        const classObj = this;
        
        return new Promise((resolve, reject) => {
            fs.createReadStream(filePath).pipe(csvParser({quote:''})).on('data', (row) => {
              // console.log(row);
                i++;
                csvFilePath[i] = row['BOL Uploaded on'];
				//console.log(row['BOL Uploaded on']);
				/* if(row['BOL Uploaded on']=="null")
				{
					row['BOL Uploaded on']="no access date";
					storedFileData.push(row);
					
				}
				else{
					console.log("hi");
				    classObj.getFileAccessDate(row['BOL Uploaded on'],function(accessDate){
					row['BOL Uploaded on']=accessDate;
					storedFileData.push(row);
					console.log(row);
					});
					
					
				} */
				//
				console.log(csvFilePath[i])
                
            }).on('end', () => {
                resolve(csvFilePath);
            });
        }); 
    }
	
	
  getFileAccessDate(path)
	{      
	         path=publicfolder+JSON.parse(path);
	         console.log("path is ",path )
			 return new Promise((resolve, reject) => {
			 var command = 'stat -c ‘%y’ '+path;
			 try{
				 exec(command, (err, stdout, stderr) => {
					  if (err) {
						//some err occurred
						console.log("errorgetFileAccessDate "+err);
						reject("error");
					  } else {
					   // the *entire* stdout and stderr (buffered)
					   console.log(`stdout: ${stdout}`);
					   resolve(stdout);
					 
					 }
				});
			 }
			 catch (err) {
				console.error(err);
				reject("error");
				}

	}

    /**
    * Prepare data for final csv file
    * @function prepareFinalCSVData
    * @return complete output object.
    */
    prepareFinalCSVData(transcriptArr){
        var output = [];
        storedFileData.forEach(function(item) {
            var prepareData = {};
            prepareData = item;
            var existing = transcriptArr.filter(function(v, i) {
                return v["Work Id"] == item["Work Id"];
            });
            if(existing.length){
                prepareData['Transcript'] = existing[0].transcript;
            }
            output.push(prepareData);
        });
        return output;
    }

    /**
    * Write Data to CSV File
    * @function writeCSVFile
    * @return null.
    */
    writeCSVFile(data){
        const csvWriter = createCsvWriter({  
        path: transcriptFilePath,
        header: [
            {id: 'Work Id', title: 'Work Id'},
            {id: 'Name', title: 'Name'},
            {id: 'Phone', title: 'Phone'},
            {id: 'Email', title: 'Email'},
            {id: 'Bot', title: 'Bot'},
            {id: 'Date Time', title: 'Date Time'},
            {id: 'Intent', title: 'Intent'},
            {id: 'Missed Utterance', title: 'Missed Utterance'},
			{id: 'Missed slot', title: 'Missed slot'},
            {id: 'Transcript', title: 'Transcript'}
        ]
        });
        csvWriter  
        .writeRecords(data)
        .then(()=> console.log('The CSV file was written successfully'));
    }

    
    /**
    * get Password from AWS SSM
    * @function getPassword
    * @return password.
    */
    getPassword(callback)
    {
        const AWS      = require('aws-sdk');
        AWS.config.loadFromPath(__basePath + '/app/config/aws-config.json');
        const ssm = new (require('aws-sdk/clients/ssm'))();
        var configParams=[];
         var  data = ssm.getParameters({Names: ['MAIL_CONFIG'] },function(err, data) {
         if (err) {
            console.log("error  in SSM "+err);
         }       
         else
         {
          console.log("data is "+JSON.stringify(data));
          callback(data);
         }
         });
    }


	 /**
    * Send CSV File via email
    * @function sendEmail
    * @return null.
    */
    sendEmail(){
    var self=this;
    self.getPassword(function(password){
            console.log("Parameters are" +password );
            console.log("password is" +password.Parameters[0].Value );
            var transporter = nodemailer.createTransport({
            host: constant.mail.host, // hostname
            secureConnection: false, // TLS requires secureConnection to be false
            port: constant.mail.port, // port for secure SMTP
            tls: {
            ciphers:'SSLv3'
            },
            auth: {
                user: constant.mail.from,
                pass: password.Parameters[0].Value
            }
            });
            var opts = {
            from: constant.mail.from,
            to: constant.mail.to,
            subject: constant.mail.healthReport.subject,
            html: constant.mail.healthReport.mailBody,
            attachments: [{   // filename and content type is derived from path
                path: transcriptFilePath
            }]
            }
            transporter.sendMail(opts, function(err, info){
            if(err){
            console.error(err)
            }
            console.log('Mail sent', info);
            });

        });
    }



    /**
    * Generate Chatbot Health Report
    * @function generateReport
    * @return null.
    */
    async generateReport(){
        const classObj = this;
        const workIdObj = await this.readCSVFile();
      //  console.log("storedFileData is:"+JSON.stringify(storedFileData));
       /*  var formatedData=this.updateKeyOfRecords();
        await this.updateBOLReport(formatedData); 
		var finalData = await this.prepareFinalCSVData(completeResult);
        await this.writeCSVFile(finalData);
		await this.sendEmail(); */
    }
}

var obj = new ReportGenerator();
obj.generateReport();