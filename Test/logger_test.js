global.__basePath = process.cwd() + '/';
const basePath = __basePath;
const winston           = require('winston');
const moment            = require('moment');
const nodeEnv           = process.env.NODE_ENV || 'development';
const csvFilePath         = __basePath+"logs/" +"BOL-Analysis-Test" + '-' + moment().format('YYYY-MM-DD') + '.log';


const loggerCSV = new (winston.Logger)({
    emitErrs  : false,
    transports: [
        new (winston.transports.File)(
            {      
            filename       : csvFilePath,
            json           : false,
            formatter      : function (args) {
                                return args.message;
                            }
        })
    ]
});

const logger = new (winston.Logger)({
    emitErrs  : false,
    transports: [
        
        new (winston.transports.File)(
            {      
            level          : "debog",
            label          : "Log",
            name           : 'log_file',
            filename       : __basePath + '/logs/'+ nodeEnv + '-' + moment().format('YYYY_MM_DD') + '.log',
            handleException: true,
            json           : false,
            maxSize        : 52428800,
            maxFiles       : 10,
            prettyPrint    : true,
            formatter      : function (options) {
                return "[" + moment().format('YYYY-MM-DD HH:mm:ss') + "] " +
                    options.label + "." + options.level.toUpperCase() + ": " + (options.message ?
                                                                                options.message :
                                                                                '') +
                    (options.meta && Object.keys(options.meta).length ?
                     '\n' + JSON.stringify(options.meta, null, 4) :
                     '');
            }
        })
    ]
});

const loggerLoadData = new (winston.Logger)({
    emitErrs  : false,
    transports: [
        
        new (winston.transports.File)(
            {      
            level          : "debog",
            label          : "Log",
            name           : 'log_file',
            filename       : basePath + 'logs/'+ nodeEnv + '-' + moment().format('YYYY_MM_DD') + '.log',
            handleException: true,
            json           : false,
            maxSize        : 52428800,
            maxFiles       : 10,
            prettyPrint    : true,
            formatter      : function (options) {
                return "[" + moment().format('YYYY-MM-DD HH:mm:ss') + "] " +
                    options.label + "." + options.level.toUpperCase() + ": " + (options.message ?
                                                                                options.message :
                                                                                '') +
                    (options.meta && Object.keys(options.meta).length ?
                     '\n' + JSON.stringify(options.meta, null, 4) :
                     '');
            }
        })
    ]
});


const loggerRemoveData = new (winston.Logger)({
    emitErrs  : false,
    transports: [
        
        new (winston.transports.File)(
            {      
            level          : "debog",
            label          : "Log",
            name           : 'log_file',
            filename       : basePath + 'logs/RemoveData'+ nodeEnv + '-' + moment().format('YYYY_MM_DD') + '.log',
            handleException: true,
            json           : false,
            maxSize        : 52428800,
            maxFiles       : 10,
            prettyPrint    : true,
            formatter      : function (options) {
                return "[" + moment().format('YYYY-MM-DD HH:mm:ss') + "] " +
                    options.label + "." + options.level.toUpperCase() + ": " + (options.message ?
                                                                                options.message :
                                                                                '') +
                    (options.meta && Object.keys(options.meta).length ?
                     '\n' + JSON.stringify(options.meta, null, 4) :
                     '');
            }
        })
    ]
});

var writeLogError = function(name, errMsg, errStatusCode, errStack){
    var error_json = {
        functionName: name,
        message: errMsg,
        statusCode: errStatusCode,
        stack: errStack
    }
    logger.error(error_json);
}

var writeLogInfo = function(name, msg, apiResponse){
	
    var info_json = {
        functionName: name,
        message: msg,
        info: apiResponse
    }
    logger.info(info_json);
}


var writeLogLoadDataError = function(name, errMsg, errStatusCode, errStack){
    var error_json = {
        functionName: name,
        message: errMsg,
        statusCode: errStatusCode,
        stack: errStack
    }
    loggerLoadData.error(error_json);
}

var writeLogLoadDataInfo = function(name, msg, apiResponse){
    var info_json = {
        functionName: name,
        message: msg,
        info: apiResponse
    }
    loggerLoadData.info(info_json);
}
var writeLogRemoveDataError = function(name, errMsg, errStatusCode, errStack){
    var error_json = {
        functionName: name,
        message: errMsg,
        statusCode: errStatusCode,
        stack: errStack
    }
    loggerRemoveData.error(error_json);
}

var writeLogRemoveDataInfo = function(name, msg, apiResponse){
    var info_json = {
        functionName: name,
        message: msg,
        info: apiResponse
    }
    loggerRemoveData.info(info_json);
}


var writeCSVInfo = function(msg){
    loggerCSV.info(msg);
}


 /** Return Logger instances */
 module.exports = {
    logger          : logger,
	csvFilePath:csvFilePath,
	loggerLoadData  : loggerLoadData,
	loggerRemoveData:loggerRemoveData,
	writeLogRemoveDataInfo: writeLogRemoveDataInfo,
	writeLogRemoveDataError:writeLogRemoveDataError,
	writeLogLoadDataError:writeLogLoadDataError,
	writeLogLoadDataInfo:writeLogLoadDataInfo,
    writeLogError   : writeLogError,
    writeLogInfo    : writeLogInfo,
    writeCSVInfo    : writeCSVInfo
 };