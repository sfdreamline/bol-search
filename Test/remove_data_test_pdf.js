const fs = require('fs')
const path = require('path')
global.__basePath = process.cwd() + '/';
const esConnection = require('./server/connection-test-pdf')
const util = require('util') 
const pdfFolder = 'unidexed-test';
var recursive = require("recursive-readdir");
//const destinationFolder="unidexed-test"
const failedFolder="failed-test";
const imgFolder = '/images-test/';
const rootPath="/home/ubuntu/SearchPdf";
const tesseract = require("node-tesseract-ocr")
const exec = require('await-exec');
const moveFile = require('move-file');
const makeDir = require('make-dir');
const textractScan = require("./textractUtils");
const loggerObj = require('./logger');
const search = require('./server/search-test')
const config = {
  lang: "eng",
  oem: 1,
  psm: 3,
}

/** read pdf directory */
/* async function readDirectory(pdfFolder,callback) { 
	  fs.readdir(pdfFolder, (err, files) => {
		  
		 callback(files)
	   });
} */


async function readDirectory(pdfFolder,callback)
{       
		recursive(pdfFolder, async function (err, files) {
			callback(files);
		  // `files` is an array of file paths
		   /* for(var i=0;i<files.length;i++)
		   {
			  if(path.extname(files[i])!=".pdf")
			    (files);
		   } */
		})
};

function esacpedFilename (str)
{
	//console.log("esacpedFilename is" , str)
	var res=str.replace("(", "\\(");
	res=res.replace(")", "\\)");
	//console.log("esacpedFilename after" , res)
	return res;
}

function unesacpedFilename (str)
{
	//console.log("esacpedFilename is" , str)
	var res=str.replace("\\(", "(");
	res=res.replace("//)", ")");
	//console.log("esacpedFilename after" , res)
	return res;
}

function removeBackSlash(name)
{
	return name.replace(/\\/g, '');
}

function getFilename (str)
{
	//console.log("getPNGFilename is" , str)
	//gives the only the name of file without path
	var res=str.replace(/^.*[\\\/]/, '')
	console.log("getFilename is" , res)
	return res;
}
  
async function makeFolder(path)  {
	
	console.log("creating folder structure for path ",path);
    await makeDir(path);
    console.log(path);
}

async function moveFiles(filename,destination){
	console.log("move files , filename is ",filename +"  and destination is "+ destination);
	//await makeFolder(destination);
    await moveFile(path.join(rootPath,filename), destination);
    console.log(filename,'has been moved to '+destination);
}

async function checkConnection()
{
    await esConnection.checkConnection();  
}

async function processFile(filename)
{
         	console.log("in process file for unindexing and filename is "+ filename)
	
	            
				await search.deleteDocument(getFilename(filename)) 
		        /* var destination = path.join(rootPath,destinationFolder,filename);
				console.log("destination to move after undexing is ",destination);
				await moveFiles(filename,destination) */
			   
}	
  
/** Clear ES index, parse and index all files from the books directory */
  
    // Read files directory
    readDirectory(pdfFolder, async function(filenames){	
   try {
    // Clear previous ES index
	 console.time();
	await esConnection.checkConnection();
   // await esConnection.resetIndex()
    //console.log("filenames ",filenames);
      for(var i=0;i<filenames.length;i++)
      {
		    var filename=filenames[i];
            console.log(filename);
            if(path.extname(filename)==".pdf")
            {
				
              console.log("pdf file found to unindex ");
			  await processFile(filename);
            }
            else 
            {
              console.log("not a pdf file ",filename);
            }
      } 
	  console.timeEnd();
   }
   catch (err) {
    console.error(err)
  }
    }) ; 
module.exports =  {
    processFile    : processFile
 };