const aws = require("aws-sdk");

aws.config.loadFromPath('aws-config.json');
/* 
aws.config.update({
  accessKeyId: config.awsAccesskeyID,
  secretAccessKey: config.awsSecretAccessKey,
  region: config.awsRegion
}); */

const textract = new aws.Textract();


module.exports = async buffer => {
  const params = {
    Document: {
      /* required */
      Bytes: buffer
    }
  };

  const request = textract.detectDocumentText(params);
  const data = await request.promise();
  //console.log("data is ",data);

  if (data && data.Blocks) {
    //const { keyMap, valueMap, blockMap } = getKeyValueMap(data.Blocks);
    //const keyValues = getKeyValueRelationship(keyMap, valueMap, blockMap);
    return data.Blocks
  }

  // in case no blocks are found return undefined
  return undefined;
};