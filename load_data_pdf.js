const fs = require('fs');
const path = require('path')
global.__basePath = process.cwd() + '/';
const esConnection = require('./server/connection-pdf')
const util = require('util') 
//const pdfFolder = 'pdf';
const permissionFolder = '/home/bol/ftp/files';
const tempFolder="/home/bol/ftp/files/Scan_temp";
const pdfFolder = '/home/bol/ftp/files/Scan';
const publicPDFFolder="pdf"
var recursive = require("recursive-readdir");
const destinationFolder="public"
const failedFolder="failed";
const imgFolder = '/images/';
const rootPath="/home/ubuntu/SearchPdf";
const FTPRootPath="home/bol/ftp/files/Scan"
const tesseract = require("node-tesseract-ocr")
const exec = require('await-exec');
const moveFile = require('move-file');
const makeDir = require('make-dir');
const textractScan = require("./textractUtils");
const loggerObj = require('./logger');
const moment            = require('moment');
const todaysBackupIndexName= esConnection.index+"backup"+moment().format('DD_MM_YYYY');
const yesterdayBackupIndexName= esConnection.index+"backup"+moment().subtract(1, "days").format('DD_MM_YYYY');

const config = {
  lang: "eng",
  oem: 1,
  psm: 3,
}

/** read pdf directory */
/* async function readDirectory(pdfFolder,callback) { 
	  fs.readdir(pdfFolder, (err, files) => {
		  
		 callback(files)
	   });
} */
/**
    * Provide access to FTP folder
    * @function provideAccess
    * @return null.
    */
	async function provideAccess()
	{
			return new Promise((resolve, reject) => {
			const { exec } = require('child_process');
			exec('sudo chmod -R 777 '+permissionFolder, (err, stdout, stderr) => {
			  if (err) {
				//some err occurred
				console.log("error in providing access to folder "+permissionFolder +" "+err);
				reject("access not given  ");
			  } else {
			   // the *entire* stdout and stderr (buffered)
			//   console.log(`stdout: ${stdout}`);
			//   console.log(`stderr: ${stderr}`);
			   resolve("access provided ");
			  }
			});
		});
    }
	
async function takeBackup(indexName)
	{
			return new Promise((resolve, reject) => {
			const { exec } = require('child_process');
			var command= 'curl -X PUT "http://localhost:9200/_snapshot/backup/' +indexName+'?wait_for_completion=true&pretty"'
			+ " -H 'Content-Type: application/json' -d'" 
			+'{"indices": '+'"'+esConnection.index+'",'
			+'"ignore_unavailable": true,  "include_global_state": false,  "metadata": {    "taken_by": "Shankar","taken_because": "Backup of the index named dreamlinetest" }}' 
			+"'";

			  exec(command, (err, stdout, stderr) => {
			  if (err) {
				//some err occurred
				console.log("error in taking backup "+err);
				reject(false);
			  } else {
			   // the *entire* stdout and stderr (buffered)
			   console.log(`stdout: ${stdout}`);
			   console.log(`stderr: ${stderr}`);
			   resolve(true);
			  }
			});
		});
    }	
	
async function deletePreviousBackup()
	{
			return new Promise((resolve, reject) => {
			const { exec } = require('child_process');
			var command= 'curl -X DELETE "http://localhost:9200/_snapshot/backup/' +yesterdayBackupIndexName+'?pretty"'

			 exec(command, (err, stdout, stderr) => {
			  if (err) {
				//some err occurred
				console.log("error in deleting backup "+err);
				reject("not able to delete  ");
			  } else {
			   // the *entire* stdout and stderr (buffered)
			   console.log(`stdout: ${stdout}`);
			   console.log(`stderr: ${stderr}`);
			   resolve("backup deleted   ");
			  }
			});
		});
    }	

async function checkIfBackupExits(indexName)
	{
			return new Promise((resolve, reject) => {
			const { exec } = require('child_process');
			var command= 'curl -X GET "http://localhost:9200/_cat/snapshots/backup?v&s=id&format=JSON" '

			 exec(command, (err, stdout, stderr) => {
			  if (err) {
				//some err occurred
				console.log("error in checkIfTodaysBackupExits "+err);
				reject("error in checkIfTodaysBackupExits  ");
			  } else {
			   // the *entire* stdout and stderr (buffered)
			   console.log(`stdout: ${stdout}`);
			   var res=JSON.parse(stdout);
			  for(var i = 0; i < res.length; i++) {
				//	console.log("ID IS ",res[i]);
					if(res[i].id==indexName)
					{
						console.log("backup exists ",indexName);
						resolve(true);
					}
				}
				resolve(false);
			  // console.log(`stderr: ${stderr}`);
			  
			  }
			});
		});
    }	
	
async function cleanEmptyFoldersRecursively(folder) {
    var fs = require('fs');
    var path = require('path');

    var isDir = fs.statSync(folder).isDirectory();
    if (!isDir) {
      return;
    }
    var files = fs.readdirSync(folder);
    if (files.length > 0) {
      files.forEach(function(file) {
        var fullPath = path.join(folder, file);
        cleanEmptyFoldersRecursively(fullPath);
      });

      // re-evaluate files; after deleting subfolder
      // we may have parent folder empty now
      files = fs.readdirSync(folder);
    }

    if (files.length == 0 && folder!=pdfFolder) {
      console.log("removing: ", folder);
      fs.rmdirSync(folder);
      return;
    }
  }
  
async function readDirectory(pdfFolder,callback)
{       
		await provideAccess();
		if(!await checkIfBackupExits(todaysBackupIndexName))
		{
			console.log("taking backup as no backup for today exists ");
		 if (await takeBackup(todaysBackupIndexName))
		 {
			console.log("backup done now check if previous day back exists ");
			if(await checkIfBackupExits(yesterdayBackupIndexName))
				{
				 console.log("deleting previous day back  ",yesterdayBackupIndexName);
				 await deletePreviousBackup();
				}
			else{
				console.log("There is no previos day backup  ");	
				}
		 }
		}
		await cleanEmptyFoldersRecursively(pdfFolder);
		await convertFileNameWithoutSpace();
		checkIfDirectoryExists( async function(exists){
			if(!exists)
				{
					checkIfDirectoryIsEmpty( async function(exists){
						if(!exists)
						{
							await createDateFolderAndMoveFiles();
							recursive(pdfFolder, async function (err, files) {
									callback(files);
								  // `files` is an array of file paths
								   /* for(var i=0;i<files.length;i++)
								   {
									  if(path.extname(files[i])!=".pdf")
										(files);
								   } */
								});
						}
					});
				}
			});	
};

function checkIfDirectoryExists(callback)
{
	fs.access(pdfFolder+"/"+moment().format('DD_MM_YYYY'), function(error) {
	  if (error) {
		console.log("Directory does not exist.")
		callback(false);
	  } else {
		console.log("Directory exists.")
		callback(true);
	  }
	});
}

function checkIfDirectoryIsEmpty(callback)
{
	var dirname=pdfFolder;
		fs.readdir(dirname, function(err, files) {
				if (err) {
					console.log("Directory does not exist.")
				  callback(false);
				} else {
					console.log("Directory exists.")
				   if (files.length) {
					   
					   console.log("Directory exists and have files .")
					   callback(false);
				   }
				   else{
					   console.log("Directory exists but empty .")
					  callback(true); 
				 }
				}
			});
}

async function createDateFolderAndMoveFiles()
	{	
			return new Promise((resolve, reject) => {
			const { exec } = require('child_process');
			var dateFolder=pdfFolder+"/"+moment().format('DD_MM_YYYY');
			
			var command='mv '+pdfFolder+"/* "+tempFolder+" ;"+'mkdir  '+dateFolder+" ;"+'mv '+tempFolder+"/* "+dateFolder;
			exec(command, (err, stdout, stderr) => {
					  if (err) {
						//some err occurred
						console.log("error in moving and creating date folder "+err);
						reject("not able to move  ");
					  } else {
					   // the *entire* stdout and stderr (buffered)
					   console.log(`stdout: ${stdout}`);
					  // console.log(`stderr: ${stderr}`);
					   resolve("date folder created success ");
					  }
				});
			});
    }
function esacpedFilename (str)
{
	//console.log("esacpedFilename is" , str)
	var res=str.replace("(", "\\(");
	res=res.replace(")", "\\)");
	//console.log("esacpedFilename after" , res)
	return res;
}

function unesacpedFilename (str)
{
	//console.log("esacpedFilename is" , str)
	var res=str.replace("\\(", "(");
	res=res.replace("//)", ")");
	//console.log("esacpedFilename after" , res)
	return res;
}

function removeBackSlash(name)
{
	return name.replace(/\\/g, '');
}

function getFilename (str)
{
	//console.log("getPNGFilename is" , str)
	//gives the only the name of file without path
	var res=str.replace(/^.*[\\\/]/, '');
		//console.log("getFilename is" , res)
	return res;
}

function getNumericValuefromfileName(str)
{
	str = path.basename(str, '.pdf');
	var res= str.match(/\d/g);
	if(res)
	{	
	res = res.join("");
	}
	else{
		var arr = "abcdefghijklmnopqrstuvwxyz".split("");
	    var result= str.replace(/[a-z]/ig, function(m){ return arr.indexOf(m.toLowerCase()) + 1 });	
		res= result.match(/\d/g);
		res = res.join("");
	}
	console.log("getNumericValuefromfileName is" , res)
  return res;
}

/** covert pdf to png */
async function convert(filename)
	{      
	         console.log("converting the png and file is" , filename)
	         var pdfPath=path.join(rootPath,esacpedFilename(filename))
			 await makeFolder(path.join(rootPath,imgFolder,path.dirname(filename)));
			 var imgPath=path.join(rootPath,imgFolder,(esacpedFilename(filename).replace(".pdf", ".png")));
			 var command = "sudo pdftoppm -png -rx 300 -ry 300 " +esacpedFilename(filename) +">" +imgPath ;
			 try{
				 await exec(command);
			 }
			 catch (err) {
				console.error(err);
			   await moveFiles(filename,failedFolder)
				}
			 return imgPath;

	}
	
/** covert pdf to png */
async function convertFileNameWithoutSpace()
	{      
	       //  console.log("converting the sapce if in the file names " )
			 var command = 'find '+pdfFolder +' -name "* *" -type f '+"| rename 's/ //g'";
			 try{
				 await exec(command);
			 }
			 catch (err) {
				console.error(err);
				}

	}	
/** delete png files*/
async function deletePNGFiles()
	{      
	         console.log("deleting png files " )
			 var command = 'rm -rf  '+imgFolder.replace("\/", "") +'* ';
			 try{
				// console.log("command is ",command)
				 await exec(command);
			 }
			 catch (err) {
				console.error(err);
				}

	}	
		
	
	
 /** get paragraph from OCR Text */
function getParagraph (OCRText) {
    const title="BOL";
    const author="Dreamline";
    // Clean OCRText and split into array of paragraphs
    const paragraphs = OCRText.split(/\n\s+\n/g) // Split each paragraph into it's own array entry
      .map(line => line.replace(/\r\n/g, ' ').trim()) // Remove paragraph line breaks and whitespace
      .map(line => line.replace(/_/g, '')) // Guttenberg uses "_" to signify italics.  We'll remove it, since it makes the raw text look messy.
      .filter((line) => (line && line !== '')) // Remove empty lines
  
   // console.log(`Parsed ${paragraphs.length} Paragraphs\n`)
    return paragraphs;
  }	

function trimPraragraphs(paragraphs)
{
	  var searchwords =["Emergency Contact Information","Where the rate is","RECEIVED, subject to","SPECIAL INSTRUCTIONS:"];
	for(var i=0;i<paragraphs.length;i++)
	{		
       // console.log("searching in pargaraph ",paragraphs[i])
        for(var j=0;j<searchwords.length;j++)
		{
		//console.log("searching for  word  ",searchwords[j]);
		var position=paragraphs[i].search(searchwords[j])
		 if(position!=-1)
		 {
			// console.log(" matched search word is ",searchwords[j])
			 paragraphs[i]=paragraphs[i].slice(0,position);
			 //and remove below paragraphs
			 paragraphs.length=i+1;
			 break;
		 }
		 
		}
	} 
		return paragraphs;
}

/** get OCR Text AWS*/
async function getOCRTextAWS(filePath)
	{
	filePath=removeBackSlash(filePath);
	var data = fs.readFileSync(filePath);
	const results = await textractScan(data);
	//console.log("results in getOCRTextAWS",results);
	   const title="BOL";
	   const author="Dreamline";
	   var  paragraph=getParagraphs(results);
	    //console.log("befor trimming paragraphs ",paragraph);
		const paragraphs=trimPraragraphs(paragraph);
	  // console.log(" trimmed paragraphs ",paragraphs);
	   //console.log("paragraphs are ",paragraphs)
	   return ({title,author,paragraphs }); 

	}

  /** get OCR Text */
  async function getOCRText(filename)
  {
	  filename=removeBackSlash(filename);
	  console.log("filename in getOCRText",filename);
	return  new Promise(async function(resolve, reject) { 
		  await tesseract.recognize(filename, config)
			.then(text => {
			  console.log("Result:", getParagraph(text));
			  const title="BOL";
			  const author="Dreamline";
			  const paragraphs=getParagraph(text);
			  console.log("befor trimming paragraphs ",paragraphs);
			  paragraphs=trimPraragraphs(paragraphs);
			//  console.log(" trimmed paragraphs ",paragraphs);
			   resolve ({title,author,paragraphs });
			 // return(getParagraph(text));
			 
			})
			.catch(async error => {
				console.log("ocrText error",error.message)
			  //await moveFiles(filename,failedFolder)
			  
			})
    }); 
  } 

 
async function makeFolder(path)  {
	
	console.log("creating folder structure for path ",path);
    await makeDir(path);
    console.log(path);
}

async function moveFiles(filename,destination){
	//console.log("move files , filename is ",filename +"  and destination is "+ destination);
	//await makeFolder(destination);
	var  source="";
	if(filename.includes("home"))
	{
		source =filename;
		destination=destination.replace(FTPRootPath,publicPDFFolder);
		//console.log(" if, source  ",source +"  and destination is "+ destination);
	}
	else{
		source =path.join(rootPath,filename);
		//console.log(" else , source ",source +"  and destination is "+ destination);
	}	
    await moveFile(source, destination);
    console.log(filename,'has been moved to '+destination);
}

/** Bulk index the book data in ElasticSearch */
async function insertBookData (title,titleNum, author, paragraphs,filePath) {
  let bulkOps = [] // Array to store bulk operations
 if(paragraphs.length>0)
  {  
  // Add an index operation for each section in the book
  for (let i = 0; i < paragraphs.length; i++) {
    // Describe action
    bulkOps.push({ index: { _index: esConnection.index, _type: esConnection.type } })

    // Add document
    bulkOps.push({
      author,
      title,
	  titleNum,
	  filepath: filePath,
      location: i,
      text: paragraphs[i]
    })

    if (i > 0 && i % 500 === 0) { // Do bulk insert after every 500 paragraphs
      await esConnection.client.bulk({ body: bulkOps })
      bulkOps = []
      console.log(`Indexed Paragraphs ${i - 499} - ${i}`)
    }
  }

  // Insert remainder of bulk ops array
  await esConnection.client.bulk({ body: bulkOps })
  console.log(`Indexed Paragraphs ${paragraphs.length - (bulkOps.length / 2)} - ${paragraphs.length}\n\n\n`)
  }
}

function getParagraphs(results) {
    //items += toString(items)
	//console.log(results[1]);
	//var result=results.map(getFullText)
	var maxParaLength=150;
	
	var actualTotalLength=results.length;
	// console.log("results.length ",results.length);
	var parargraphs=[];
	var tempString=""
		  results.forEach(function(block) {
		 if(block.Text && block.BlockType=="LINE" )
			 
			// console.log(block.Text);
			 tempString=tempString+block.Text +" "; 
			if(tempString.length>maxParaLength)
			 {
				 parargraphs.push(tempString);
				 tempString="";
			 }			 
		
		});
	return parargraphs;
    
  }


function getFullText(item) {
    //items += toString(items)
	//console.log(item);
    if(item.Text) { 
        return item.Text 
     }
    
  }
  
  async function checkConnection()
  {
    await esConnection.checkConnection();  
  }
async function processFile(filename)
{
	console.log("in process file and filename is "+ filename)
	var imgPath= await convert((filename));
			  console.log("file converted to image and path is  ",imgPath)
             // var result=await getOCRText(imgPath);	
			  var result=await getOCRTextAWS(imgPath)
			 // console.log("data is ",data);
			 // var result=getParagraphs(data);
				//   console.log("result ",result+" and result length"+result.length);
			  if(result.length!=0)
			   {   
		        var destination = path.join(rootPath,destinationFolder,filename);
			//	console.log("destination is ",destination);
              var sourcefilepath="";
				if(filename.includes("home"))
				{
					sourcefilepath=filename.replace(FTPRootPath+"/",publicPDFFolder+"/");
				//	console.log(" sourcefilepath if ",sourcefilepath );
				}
				else{
					sourcefilepath=filename;
					//console.log(" sourcefilepath else ",sourcefilepath );
				}
				
                await insertBookData(getFilename(filename),getNumericValuefromfileName(filename), result.author, result.paragraphs,sourcefilepath );

			   await moveFiles(filename,destination)
			   }
			   else{
				   console.log("paragraphs are empty");
				   await moveFiles(filename,failedFolder)
			   }
}	
  
/** Clear ES index, parse and index all files from the books directory */
  
    // Read files directory
    readDirectory(pdfFolder, async function(filenames){	
   try {
    // Clear previous ES index
	 console.time();
	 
	await esConnection.checkConnection();
    //console.log("filenames ",filenames);
      for(var i=0;i<filenames.length;i++)
      {
		    var filename=filenames[i];
            console.log(filename);
            if(path.extname(filename)==".pdf")
            {
				
              console.log("pdf file found now convert ");
			  await processFile(filename);
            }
            else 
            {
              console.log("not a pdf file ",filename);
            }
      } 
	  await deletePNGFiles();
	  await cleanEmptyFoldersRecursively(pdfFolder);
	  console.timeEnd();
   }
   catch (err) {
    console.error(err)
  }
    }) ; 
module.exports =  {
    processFile    : processFile
 };