global.__basePath = process.cwd() + '/';

let mongoose = require('mongoose');
var momenttz = require('moment-timezone');
let HealthReportModel = require(__basePath+'/app/module/db/HealthReportSchema');
const loggerObj = require(__basePath + '/app/config/logger');
var  database = 'DreamlineChatbot';
const username="DreamlineBotUser";
const password="Dreamline%40BOT";


class Database {
	
	constructor() {
		//this.connect()
	  }	
  
	/**
    ** @function _connect
    * @return connects with monggo db .
    */ 
	 connect() { 
	// console.log("connect method for mongo db ");
	 loggerObj.writeLogInfo('connect', 'connect method for mongo db', "");
	// Build the connection string 
	var dbURI = 'mongodb://'+username+':'+password+'@localhost/'+database;
			mongoose.set('useNewUrlParser', true);
			mongoose.set('useFindAndModify', false);
			mongoose.set('useCreateIndex', true);
			mongoose.set('useUnifiedTopology', true);
			// Create the database connection 
			mongoose.connect(dbURI);
			var db = mongoose.connection;
			db.once('open', function(){
				console.log('db connection open');
				loggerObj.writeLogInfo('connect', 'db connection open', "");
			  });
			  
			  mongoose.connection.on('connected',function () { 
			  console.log('Mongoose default connected ' );
			  loggerObj.writeLogInfo('_checkForIncomingChats', "Mongoose default connection connected", "");
			  
			});
					// If the connection throws an error
			mongoose.connection.on('error',function (err) { 
			  console.log('Mongoose default connection error: ' + err);
			  loggerObj.writeLogError('_checkForIncomingChats', "Mongoose default connection error", 500, err);
			}); 

			// When the connection is disconnected
			mongoose.connection.on('disconnected', function () { 
			  console.log('Mongoose default connection disconnected'); 
			  loggerObj.writeLogInfo('connect', 'Mongoose default connection disconnected', "");
			});

			// If the Node process ends, close the Mongoose connection 
			process.on('SIGINT', function() {   
			  mongoose.connection.close(function () { 
				console.log('Mongoose default connection disconnected through app termination'); 
				loggerObj.writeLogInfo('connect', 'Mongoose default connection disconnected through app termination', "");
				process.exit(0); 
			  }); 
			});   
	  
	 } 
  
	
	/**
    ** @function _checkIfRecordExists
    ** check if the record exists or not and based on that
	** call the update or save method
    */
  
 async _checkIfRecordExists(model,botName,intentName)
  {
	 loggerObj.writeLogInfo('_checkIfRecordExists', '_checkIfRecordExists method invoked',"" ); 
	 var self=this;
	 console.log("in checkIfRecordExists")
	 return new Promise((resolve, reject) => { 
	  model.find({
			createdAt: momenttz().tz("America/New_York").format('YYYY-MM-DD'),
			intent:  intentName
		  }).then(doc => {
			 loggerObj.writeLogInfo('_checkIfRecordExists', 'find query result',doc); 
			// console.log("find query result"+doc);
			 resolve(doc);
		  })
		  .catch(err => {
			//console.error(err);
			loggerObj.writeLogError('_checkIfRecordExists', ' error in checking if record already exists ', 500, err);
			reject(err);
		  }) 
	 });
  }
  
	
    /**
    ** @function _updateRecord
    ** Increment the counter value
    */
  async _updateRecord(model,botName,intentName,countValue)
  {
	    console.log("in updateRecord");
		loggerObj.writeLogInfo('_updateRecord', '_updateRecord method invoked',"" );
		var count=this.getCountName(botName);
		//console.log("count name is "+count);
	return new Promise((resolve, reject) => { 
	     model.updateOne(
			{
			  createdAt: momenttz().tz("America/New_York").format('YYYY-MM-DD'),
			  intent:  intentName // search query
			}, 
			{
			  [count]: countValue,  // field:values to update
			  updatedAt: momenttz().tz("America/New_York")
			})
		  .then(doc => {
			console.log(doc);
		    loggerObj.writeLogInfo('_updateRecord', 'updateRecord successfully in Mongo db ',"" );
			//mongoose.connection.close()
			resolve("success");
		  })
		  .catch(err => {
			//console.error(err);
			loggerObj.writeLogError('_updateRecord', ' error in updateRecord ', 500, err);
			reject("error");
		  })
	});
   }	   
  
    /**
    ** @function _saveRecord
    ** saves the new record.
    */
  
  async _saveRecord(model)
  {
	  console.log("in saveRecord")
	  return new Promise((resolve, reject) => { 
	  model.save().
	  then(doc => {
				// console.log(doc);
				 loggerObj.writeLogInfo('_saveRecord', 'saved record successfully in Mongo db ',"" );
				 resolve("success");
			   })
			   .catch(err => {
				 //console.error(err);
			    loggerObj.writeLogError('_saveRecord', ' error in saving record ', 500, err);
				 reject("error");
			   })
		});
  }
  /**
    ** @function findRecordsByDate
    ** To find the records based on date passed 
	**return monngose doc as result
    */
 
/**
    ** @function saveHealthReport
    ** saves the new record.
    */
  
   async saveHealthReport(records)
   {
	   console.log("in saveHealthReport")
	   return new Promise((resolve, reject) => { 
		HealthReportModel.insertMany(records).then(doc => {
				  console.log(doc);
				  loggerObj.writeLogInfo('saveHealthReport', 'saved record successfully in Mongo db ',"" );
				  resolve("success");
				})
				.catch(err => {
				  console.error(err);
				 loggerObj.writeLogError('saveHealthReport', ' error in saving record ', 500, err);
				  reject("error");
				}); 
	  });
   }


}

module.exports =  Database;








