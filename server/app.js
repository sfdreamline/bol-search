global.__basePath = process.cwd() + '/';
const Koa = require('koa')
const Router = require('koa-router')
const joi = require('joi')
const validate = require('koa-joi-validate')
const search = require('./search')
const app = new Koa()
const router = new Router()
//const Obserser = require('./services/observer');

// Log each request to the console
app.use(async (ctx, next) => {
  const start = Date.now()
  await next()
  const ms = Date.now() - start
 // console.log(` this ${ctx.method} ${ctx.url} - ${ms}`)
})


/* var indexObserser = new Obserser();
var unIndexObserser = new Obserser();


const pdffolder = __basePath+"pdf";
console.log("folder is ",pdffolder)
const publicfolder = __basePath+"public/pdf";
console.log("folder is ",publicfolder)

unIndexObserser.watchPublicFolder(publicfolder); */
/* indexObserser.watchPdfFolder(pdffolder);

indexObserser.on('file-added', log => {
  // print error message to console
  console.log(log.message);
});
unIndexObserser.on('file-removed', log => {
  // print error message to console
  console.log(log.message);
});
indexObserser.on('folder-added', log => {
  // print error message to console
 // console.log(log.message);
});
unIndexObserser.on('folder-removed', log => {
  // print error message to console
 // console.log(log.message);
}); */
// Log percolated errors to the console
app.on('error', err => {
  console.log('Server Error', err)
})

// Set permissive CORS header
app.use(async (ctx, next) => {
  ctx.set('Access-Control-Allow-Origin', '*')
  return next()
})

/**
 * GET /search
 * Search for a term in the library
 * Query Params -
 * term: string under 60 characters
 * offset: positive integer
 */
router.get('/search',
  validate({
    query: {
      term: joi.string().max(60).required(),
      offset: joi.number().integer().min(0).default(0)
    }
  }),
  async (ctx, next) => {
    const { term, offset } = ctx.request.query
	var date= new Date();
	if(term.includes("-"))
	{
		ctx.body = await search.queryTermHyphen(term, offset)
	}
	else{
		ctx.body = await search.queryTerm(term, offset)
	}
	if(ctx.body.hits.hits.length==1)
	{
		console.error(`${term}, ${date.toLocaleString('en-US')},${JSON.stringify(ctx.body.hits.hits[0]._source.filepath)}`);
	}
	if(ctx.body.hits.hits.length==0)
	{
		console.error(`${term}, ${date.toLocaleString('en-US')},"No match found"`);
	}
	else{
		console.error(`${term}, ${date.toLocaleString('en-US')},"Mutiple matches found" ` );
	}
  }
)

/**
 * GET /paragraphs
 * Get a range of paragraphs from the specified book
 * Query Params -
 * bookTitle: string under 256 characters
 * start: positive integer
 * end: positive integer greater than start
 */
router.get('/paragraphs',
  validate({
    query: {
      bookTitle: joi.string().max(256).required(),
      start: joi.number().integer().min(0).default(0),
      end: joi.number().integer().greater(joi.ref('start')).default(10)
    }
  }),
  async (ctx, next) => {
    const { bookTitle, start, end } = ctx.request.query
    ctx.body = await search.getParagraphs(bookTitle, start, end)
  }
)

const port = process.env.PORT || 3000

app
  .use(router.routes())
  .use(router.allowedMethods())
  .listen(port, err => {
    if (err) console.error(err)
    console.log(`App Listening on Port ${port}`)
  })
