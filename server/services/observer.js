const chokidar = require('chokidar');
const EventEmitter = require('events').EventEmitter;
const fsExtra = require('fs-extra');
const search = require('../search')
const Queue = require('bull');
const indexFiles = new Queue('indexFiles');
const unIndexFiles = new Queue('unIndexFiles');
const path = require('path')

const loadData = require('../../load_data_pdf');
const options = {
  delay: 2000, // 2 seconds 
  timeout: 10000
};

class Observer extends EventEmitter {
  constructor() {
    super();
  }
 

  watchPdfFolder(folder) {
	  const Queue1 = require('bull');
    try {
      console.log(
        `[${new Date().toLocaleString()}] watchPdfFolder for folder changes on: ${folder}`
      );

      var watcher = chokidar.watch(folder, { persistent: true });
       
      watcher.on('add', async filePath => {
		if (path.extname(filePath)==".pdf")   
        {
         // emit an event when new file has been added
         /*  this.emit('file-added', {
            message: "file added is "+filePath 
          }); */
		  const data = {
			  filename: filePath
		  };
		 
		  indexFiles.add(data, options);
		  indexFiles.process(async job1 => { 
		  console.log("insisde add process")
			  return await loadData.processFile(job1.data.filename); 
			});
        }
      });
	  watcher.on('unlink', async filePath => {
        {
         // emit an event when new file has been added
          /* this.emit('file-removed', {
            message: "file removed is "+filePath
          }); */
		 // console.log("file removed is "+filePath);
		 
		 
		}
      });
	  watcher.on('addDir', async filePath => {
        {
          //console.log(        `[${new Date().toLocaleString()}] ${filePath} has been added.`         );
          // emit an event when new file has been added
          this.emit('folder-added', {
            message: "folder added is "+filePath
          });
		}
      });
	  watcher.on('unlinkDir', async filePath => {
        {
          console.log(
            `[${new Date().toLocaleString()}] ${filePath} has been removed.`
          );
// emit an event when new file has been added
          this.emit('folder-removed', {
            message: "folder removed is "+filePath
          });
		}
      });
    } catch (error) {
      console.log(error);
    }
  }
  
 watchPublicFolder(folder) {
	  
    try {
      console.log(
        `[${new Date().toLocaleString()}] watchPublicFolder for folder changes on: ${folder}`
      );

      var watcher = chokidar.watch(folder, { persistent: true });
	 //  
      watcher.on('add', async filePath => {
		if (filePath.includes('.pdf'))   
        {
         // emit an event when new file has been added
         /*  this.emit('file-added', {
            message: "file added is "+filePath
          }); */
		 
        }
      });
	  watcher.on('unlink', async filePath => {
        { 
         // emit an event when new file has been added
          /* this.emit('file-removed', {
            message: "file removed is "+filePath
          }); */
		  var title=filePath.replace(/^.*[\\\/]/, '')
		  console.log("file removed is "+filePath+ " and title is "+title);
		  const data = {
			  title: title
		  };
		 
		  unIndexFiles.add(data, options);
		  unIndexFiles.process(async job => { 
			  return await search.deleteDocument(job.data.title) 
			});
		}
      });
	  watcher.on('addDir', async filePath => {
        {
          //console.log(        `[${new Date().toLocaleString()}] ${filePath} has been added.`         );
          // emit an event when new file has been added
          this.emit('folder-added', {
            message: "folder added is "+filePath
          });
		}
      });
	  watcher.on('unlinkDir', async filePath => {
        {
          console.log(
            `[${new Date().toLocaleString()}] ${filePath} has been removed.`
          );
// emit an event when new file has been added
          this.emit('folder-removed', {
            message: "folder removed is "+filePath
          });
		}
      });
    } catch (error) {
      console.log(error);
    }
  } 
  
  
}

module.exports = Observer;