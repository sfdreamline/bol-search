global.__basePath = process.cwd() + '/';
const Koa = require('koa')
const Router = require('koa-router')
const joi = require('joi')
const validate = require('koa-joi-validate')
const search = require('./search-test')
const app = new Koa()
const router = new Router();
const moment            = require('moment');
const fs                = require('fs');
const loggerObj         = require(__basePath + 'logger_test');
//const Obserser = require('./services/observer');

// Log each request to the console
/**
    * Write CSV File Header for Chatbot Health Report File
    * @function _writeCSVFileHeader
    * @return null
    */
   function  writeCSVFileHeader(callback){
        fs.stat(loggerObj.csvFilePath, function (err, stat) {
            if(err == null){
                //File exists
                console.log("file already exists");
                return callback(null, true);
            } else {
                var csvMessage = "Searched Word,Searched Date,Searched Time,Status,BOL Uploaded on";
                loggerObj.writeCSVInfo(csvMessage);
                console.log("Header write successfully");
                return callback(null, true);
            }
        });   
    }

app.use(async (ctx, next) => {
  const start = Date.now()
  await next()
  const ms = Date.now() - start
  console.log(`${ctx.method} ${ctx.url} - ${ms}`)
   
})

    

/* var indexObserser = new Obserser();
var unIndexObserser = new Obserser();


const pdffolder = __basePath+"pdf";
console.log("folder is ",pdffolder)
const publicfolder = __basePath+"public/pdf";
console.log("folder is ",publicfolder)

unIndexObserser.watchPublicFolder(publicfolder); */
/* indexObserser.watchPdfFolder(pdffolder);

indexObserser.on('file-added', log => {
  // print error message to console
  console.log(log.message);
});
unIndexObserser.on('file-removed', log => {
  // print error message to console
  console.log(log.message);
});
indexObserser.on('folder-added', log => {
  // print error message to console
 // console.log(log.message);
});
unIndexObserser.on('folder-removed', log => {
  // print error message to console
 // console.log(log.message);
}); */
// Log percolated errors to the console
app.on('error', err => {
  console.error('Server Error', err)
})

// Set permissive CORS header
app.use(async (ctx, next) => {
  ctx.set('Access-Control-Allow-Origin', '*')
  return next()
})

/**
 * GET /search
 * Search for a term in the library
 * Query Params -
 * term: string under 60 characters
 * offset: positive integer
 */
router.get('/search',
  validate({
    query: {
      term: joi.string().max(60).required(),
      offset: joi.number().integer().min(0).default(0)
    }
  }),
  async (ctx, next) => {
	  var ip=ctx.request.ip.replace(/::ffff:/, '')
	 console.log("ip is ",ip);
	  var geoip = require('geoip-lite');
	  var geo = geoip.lookup(ip);
	  console.log("geo",geo);
    // console.log(JSON.stringify( ctx));
	 
    const { term, offset } = ctx.request.query
	var date= new Date();
	if(term.includes("-"))
	{
		ctx.body = await search.queryTermHyphen(term, offset)
	}
	else{
		ctx.body = await search.queryTerm(term, offset)
	}
	if(term!="SON")
	{
		var csvMessage="";
		var date=date.toLocaleString('en-US',{timeZone:"America/New_York"});
		if(ctx.body.hits.hits.length==1)
		{
			console.error(`${term}, ${date},"One Match Found",${JSON.stringify(ctx.body.hits.hits[0]._source.filepath)},${geo.city}`);
			csvMessage = (`${term},${date},"One Match Found",${JSON.stringify(ctx.body.hits.hits[0]._source.filepath)}`)
			loggerObj.writeCSVInfo(csvMessage);
		}
		else if(ctx.body.hits.hits.length==0)
		{
			console.error(`${term},${date},"No match found",null`);
			csvMessage = (`${term},${date},"No match found",null`);
			loggerObj.writeCSVInfo(csvMessage);
		}
		else{
			console.error(`${term},${date},"Mutiple matches found",null` );
			csvMessage = (`${term},${date},"Mutiple matches found",null` );
			//console.log("hi",csvMessage);
			// csvMessage = "Searched Word,Searched Date,Searched Time,Status,BOL Uploaded on ";
			  loggerObj.writeCSVInfo(csvMessage);
		}
	}
    
  }
)

/**
 * GET /paragraphs
 * Get a range of paragraphs from the specified book
 * Query Params -
 * bookTitle: string under 256 characters
 * start: positive integer
 * end: positive integer greater than start
 */
router.get('/paragraphs',
  validate({
    query: {
      bookTitle: joi.string().max(256).required(),
      start: joi.number().integer().min(0).default(0),
      end: joi.number().integer().greater(joi.ref('start')).default(10)
    }
  }),
  async (ctx, next) => {
    const { bookTitle, start, end } = ctx.request.query
    ctx.body = await search.getParagraphs(bookTitle, start, end)
  }
)

const port = process.env.PORT || 3001

app
  .use(router.routes())
  .use(router.allowedMethods())
  .listen(port, err => {
    if (err) console.error(err)
    console.log(`App Listening on Port ${port}`)
writeCSVFileHeader(function (error, csvHeaderResponse){
	       
                });
  })
