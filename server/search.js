const { client, index, type } = require('./connection-pdf')

module.exports = {
  /** Query ES index for the provided term */
  queryTerm (term, offset = 0) {
	    var data  = "*"+term.toLowerCase()+"*";

    const body = {
      from: offset,
      
	  "query": {
			"wildcard": {
      "text": {
                "value": data
			}
			}},
			
	  "collapse" : {
			"field" : "titleNum" 
		},"aggs": {
"total": {
"cardinality": {
"field": "titleNum"
}
}
},
      highlight: { fields: { text: {} } }
    }
 
    return client.search({ index, type, body })
  },
  queryTermHyphen (term, offset = 0) {
	    term=term.toLowerCase();
	    var data  = term.replace(/-/g, '- ')+"*";
	    //var data  = term;
		console.log("Hyphen search string is ",data);

    const body = {
      from: offset,
      
	  "query": {
        "simple_query_string": {
            "query" : data,
            "fields" : [ "text" ],
			"default_operator": "and"
           
        }
      
    },
			
	  "collapse" : {
			"field" : "titleNum" 
		},"aggs": {
"total": {
"cardinality": {
"field": "titleNum"
}
}
},
      highlight: { fields: { text: {} } }
    }
 
    return client.search({ index, type, body })
  },
  deleteDocument (title1){
	  console.log("unindexing ",title1);
	  const body= {
           query: {
               match: { title: {query: title1,operator: 'and'} }
           }
        }
	  client.deleteByQuery({
        index,type,body  
    }, function (error, response) {
        console.log(response);
    });
  },

  /** Get the specified range of paragraphs from a book */
  getParagraphs (bookTitle, startLocation, endLocation) {
    const filter = [
      { term: { title: bookTitle } },
      { range: { location: { gte: startLocation, lte: endLocation } } }
    ]

    const body = {
      size: endLocation - startLocation,
      sort: { location: 'asc' },
      query: { bool: { filter } }
    }

    return client.search({ index, type, body })
  }
}
